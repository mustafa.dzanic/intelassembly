.section .data
br1: .long 0
br2: .long 0


.section .rodata
printf: .asciz "Unesite broj: "
scanf: .asciz "%d"
rez: .asciz "c= %d\n"

.section .text
.globl main
main:

pushl %ebp
movl %esp, %ebp
pushl $printf
call printf
add $4, %esp


pushl $br1
pushl $scanf
call scanf
addl $8, %esp
pushl $br2
pushl $scanf
call scanf
addl $8, %esp
pushl br2
pushl br1
call positive
add $8, %esp

call positive



.globl positive
positive:
pushl %ebp
movl %esp, %ebp

movl 8(%esp), %ebx
movl (%ebx), %ecx
movl 12(%esp),%ebx
movl (%ebx), %edx
pushl %edx
pushl %ecx
call addit
addl $8, %esp
cmpl $0, %eax
JG L1
movl $0, %eax
L1:
movl $1, %eax
ret


.globl addit
addit:
pushl %ebp
movl %esp, %ebp 
movl 8(%esp), %ebx
movl (%ebx), %ecx
movl 12(%esp), %ebx
movl (%ebx), %eax
add %ecx, %eax
movl %ebp, %esp
popl %ebp
ret







